/*--------------------------------------------------------------*/
/*  Projet: OFFOK                                               */
/*  File  : offok.tune.h                                        */
/*  Author: JeARZ                                      05/19/21 */
/*--------------------------------------------------------------*/
/*
 RTTTL tunes 
*/
#ifndef __OFFOK_TUNES_h__
#define __OFFOK_TUNES_h__
/*
 Used libs for playing rtttl ringtones
 */
#include <anyrtttl.h>
#include <binrtttl.h>
#include <pitches.h>
/*
 OffOk predefined variables holding rtttl tunes
 */
const char *_rIntro_    =
  "StarTrekIntro1:d=4,o=5,b=63:8f.,16a#,d#.6,8d6,16a#.,16g.,16c.6,f6";
const char *_rIntro1_   =
  "StarTrekIntro2:d=4,o=6,b=225:2b.4,4e.5,1a5,4g#.5,4e.5,4c#.5,4f#.5,2b.5,4b.5,2d#.6";
const char *_rGetIR_    =
  "StarTrekCom:d=32,o=7,b=480:d#,e";
const char *_rGetIRl_   =
  "StarTrekCom:d=32,o=7,b=180:d#,e,g,d#,g,d#,f#,e,f";
const char *_rCountKO_  =
  "Death March:d=4,o=5,b=125:c.,c,8c,c.,d#,8d,d,8c,c,8c,2c.";
const char *_rCountOKs_  =
  "LightMyFire:d=4,o=5,b=140:8b,16g,16a,8b,8d6,8c6,8b,8a,8g,8a,16f,16a,8c6,8f6,16d6,16c6,16a#,16g,8g#,8g,8g#";
const char *_rCountOK_ =
  "LightMyFire:d=4,o=5,b=140:8b,16g,16a,8b,8d6,8c6,8b,8a,8g,8a,16f,16a,8c6,8f6,16d6,16c6,16a#,16g,8g#,8g,8g#,16g,16a,8b,8c#6,16b,16a,16g,16f,8e,8f,1a,a";
const char *_rCountOK1_ =
  "tetris:d=4,o=5,b=160:e6,8b,8c6,8d6,16e6,16d6,8c6,8b,a,8a,8c6,e6,8d6,8c6,b,8b,8c6,d6,e6,c6,a,2a,8p,d6,8f6,a6,8g6,8f6,e6,8e6,8c6,e6,8d6,8c6,b,8b,8c6,d6,e6,c6,a,a";
const char *_rCountOK2_ =
  "Pacman:d=32,o=6,b=112:b.5,b.,f#.,d#.,b.,f#.,16p,16d#,c.,c.7,g.,e.,c.7,g.,16p,16e,b.5,b.,f#.,d#.,b.,f#.";

// just for fun ;-)
const char * arkanoid = 
  "Arkanoid:d=4,o=5,b=140:8g6,16p,16g.6,2a#6,32p,8a6,8g6,8f6,8a6,2g6";
  
#endif //  __OFFOK_TUNES_h__
